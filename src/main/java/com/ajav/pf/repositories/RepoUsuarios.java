package com.ajav.pf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ajav.pf.beans.Usuario;

public interface RepoUsuarios extends JpaRepository <Usuario,Integer> {
	
}
