package com.ajav.pf.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ajav.pf.beans.Registro;

public interface RepoRegistros extends JpaRepository <Registro,Integer> {
	
	@Query("select u from Registro u where u.is_emp = ?1 and (u.fecha_entrada between ?2 and ?3) and (u.fecha_salida between ?2 and ?3)")
	List<Registro>findAllRegistroByIs_empAndFechas(String is_emp, String fecha_inicial, String fecha_final);
	
	@Query("SELECT u FROM Registro u WHERE SUBSTRING(u.fecha_entrada,6,2) = ?1 AND SUBSTRING(u.fecha_salida,6,2) = ?1")
	List<Registro>findAllRegistroByMES(String mes);
	
	@Query("SELECT u FROM Registro u WHERE (u.fecha_entrada between ?1 AND  ?2) AND (u.fecha_salida between ?1 AND  ?2)")
	List<Registro>findAllRegistroByFechas(String fecha_inicial, String fecha_final);
	
	@Query("select sum(u.horas) from Registro u where u.is_emp = ?1 and substring(u.fecha_entrada,6,2) = ?2 and substring(u.fecha_salida,6,2) = ?2 group by u.is_emp")
	List<Integer>findHorasByIs_empAndMes(String is_emp, String mes);
}
