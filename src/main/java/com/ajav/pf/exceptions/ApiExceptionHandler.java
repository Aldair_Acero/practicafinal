package com.ajav.pf.exceptions;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {
	
	@ExceptionHandler(value = {CustomException.class})
	public ResponseEntity<Object> handlerCustomException(CustomException e){
		ApiException apiException = new ApiException(
					e.getMessage(),
					HttpStatus.NOT_FOUND,
					ZonedDateTime.now(ZoneId.of("Z"))
				);
		return new ResponseEntity<>(apiException,HttpStatus.NOT_FOUND);
	}

}
