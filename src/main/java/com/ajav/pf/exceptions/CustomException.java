package com.ajav.pf.exceptions;

public class CustomException extends RuntimeException{

	public CustomException(String IS,String mes) {
		super("Registros no encontrados para el IS correspondiente: "+ IS + " en el mes: " + mes);
	}
	
	public CustomException(String fechaI, String fechaF,int valor) {
		super("Registros no encontrados dentro de las fechas "+ fechaI + "/"+fechaF);
	}
	
	public CustomException(String mes) {
		super("Registros no encontrados en el mes: " + mes);
	}
}
