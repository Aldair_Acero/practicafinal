package com.ajav.pf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class ProyectFinalApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
	  return app.sources(ProyectFinalApplication.class); 
	}
	public static void main(String[] args) {
		SpringApplication.run(ProyectFinalApplication.class, args);
	}

}
