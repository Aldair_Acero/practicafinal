package com.ajav.pf.interfaces;

import java.util.List;

import com.ajav.pf.beans.Registro;

public interface RegistroService{
	
	public void insertRegistro(Registro registro);
	public List<Registro> getRegistros();
	public List<Integer>findHorasByIs_empAndMes(String is_emp, String mes);
	public List<Registro>findAllRegistroByFechas(String fecha_inicial, String fecha_final);
	public List<Registro>findAllRegistroByMES(String mes);
	public List<Registro>findAllRegistroByIs_empAndFechas(String is_emp, String fecha_inicial, String fecha_final);

}
