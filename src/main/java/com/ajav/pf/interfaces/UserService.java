package com.ajav.pf.interfaces;

import java.util.List;

import com.ajav.pf.beans.Usuario;

public interface UserService {
	public List<Usuario> getUsers();
	public void insertUser(Usuario user);
}
