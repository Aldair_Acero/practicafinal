package com.ajav.pf.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Registro {
	@Id
	int id_registro;
	@Column(name = "is_emp", length = 6)
	String is_emp;
	@Column(name = "fecha_entrada", length = 10)
	String fecha_entrada;
	@Column(name = "fecha_salida", length = 10)
	String fecha_salida;
	@Column(name="horas", length = 11)
	int horas;
	
	public Registro() {}
	

	public Registro(int id_registro, String is_emp, String fecha_entrada, String fecha_salida, int horas) {
		super();
		this.id_registro = id_registro;
		this.is_emp = is_emp;
		this.fecha_entrada = fecha_entrada;
		this.fecha_salida = fecha_salida;
		this.horas = horas;
	}
	
	public Registro(String is_emp, String fecha_entrada, String fecha_salida) {
		super();
		this.id_registro = 0;
		this.is_emp = is_emp;
		this.fecha_entrada = fecha_entrada;
		this.fecha_salida = fecha_salida;
		this.horas = 0;
	}
	
	public Registro(String fecha_entrada, String fecha_salida) {
		super();
		this.id_registro = 0;
		this.is_emp = " ";
		this.fecha_entrada = fecha_entrada;
		this.fecha_salida = fecha_salida;
		this.horas = 0;
	}



	public int getId_registro() {
		return id_registro;
	}

	public void setId_registro(int id_registro) {
		this.id_registro = id_registro;
	}

	public String getIs_emp() {
		return is_emp;
	}

	public void setIs_emp(String is_emp) {
		this.is_emp = is_emp;
	}

	public String getFecha_entrada() {
		return fecha_entrada;
	}

	public void setFecha_entrada(String fecha_entrada) {
		this.fecha_entrada = fecha_entrada;
	}

	public String getFecha_salida() {
		return fecha_salida;
	}

	public void setFecha_salida(String fecha_salida) {
		this.fecha_salida = fecha_salida;
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}
	
	public String toString() {
		return this.is_emp+" fecha entrada: " + this.fecha_entrada+" fecha salida: " + this.fecha_salida+" horas: " + this.horas+" id: " + this.id_registro;
	}
	
}
