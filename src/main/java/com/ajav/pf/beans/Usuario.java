package com.ajav.pf.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Usuario {
	
	@Id
	private int id_usuario;
	@Column(name = "is_usuario", length= 6)
	private String is_usuario;
	@Column(name = "nombre", length = 60)
	private String nombre;
	
	public Usuario() {}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getIs_usuario() {
		return is_usuario;
	}

	public void setIs_usuario(String is_usuario) {
		this.is_usuario = is_usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Usuario(int id_usuario, String is_usuario, String nombre) {
		super();
		this.id_usuario = id_usuario;
		this.is_usuario = is_usuario;
		this.nombre = nombre;
	}
	
}