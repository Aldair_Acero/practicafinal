package com.ajav.pf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Date;

import com.ajav.pf.beans.Registro;
import com.ajav.pf.interfaces.RegistroService;

@RestController
@RequestMapping("/archivo")
public class ArchivoController {
	
	private   String FILE_NAME = "C:/apache-tomcat-8.5.42/work/RegistrosMomentum.xls";
	private   int COL_NAME = 2;
	private   int COL_DATE = 4;
	private   int COL_CHECK = 6;
	
	@Autowired
	private RegistroService registroService;
	
	@GetMapping
	public boolean lecturaArchivo() {
		 int rowCount = 0;
	        int colCount = 0;
	        int idContador = 1;
	        Date dateAnt = new Date();
	        Date dateAct = new Date();
	        String dateActString="";
	        String dateAntString="";
	        String currentIS="";
	        String IS = "";
	        String check="";
	        int horasTrabajadas=0;
	        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
	        //ft.parse("");
			 try {

		            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
		            Workbook workbook = new HSSFWorkbook(excelFile);
		            Sheet datatypeSheet = workbook.getSheetAt(0);
		            Iterator<Row> iterator = datatypeSheet.iterator();
		            
		            while (iterator.hasNext()) {

		                Row currentRow = iterator.next();
		                Iterator<Cell> cellIterator = currentRow.iterator();
		                System.out.println("vuelta: "+ rowCount);
		                while (cellIterator.hasNext()) {
		                	
		                    Cell currentCell = cellIterator.next();
		                    if (rowCount != 0 && currentCell.getCellType() == CellType.STRING &&(colCount == COL_NAME || colCount == COL_DATE || colCount == COL_CHECK)) {
		                        if(colCount == COL_NAME) {
		                        	
		                        	IS = currentCell.getStringCellValue();
		                        	
		                        }else if(colCount == COL_DATE) {
		                        	dateAntString = dateActString;
		                        	dateActString = currentCell.getStringCellValue();
		                        	dateAnt = dateAct;
		                        	dateAct = ft.parse(dateActString);
		                        }else {
		                   
		                        	check = currentCell.getStringCellValue(); 
		                        }
		                    }
		                    colCount++;
		                }
		                
		                if(!IS.equals(currentIS)) { //verifica que si se ha cambiado de empleado
	                		System.out.println("--------cambio de empleado-----------");
	                		currentIS = IS;
	                	}
		                
		                if(rowCount != 0) {
			                if(dateAct.getDay() == dateAnt.getDay() && dateAct.getMonth() == dateAnt.getMonth()) {
		                		horasTrabajadas = (int) ((dateAct.getTime() - dateAnt.getTime()) / (60*60*1000));
		                		System.out.println("Horas trabajadas: " + horasTrabajadas);
		                		System.out.println(currentIS +" "+ dateActString +" "+check);
		               
		                		if(horasTrabajadas<20 && horasTrabajadas > 0) {registroService.insertRegistro(new Registro(idContador,currentIS,dateAntString,dateActString,horasTrabajadas));
		                		//guardamos en la base de datos pero no guardamos el encabezado del excel
		                		idContador++;
		                		}
		                	}else if(!check.equals("C/In")){
		                		System.out.println("Error de registro");
		                	}
		                }
		                
		                
		                System.out.println(currentIS +" "+ dateActString +" "+check);
		                colCount = 0;
		                
		                System.out.println();
		                rowCount++;
		            }
		        } catch (FileNotFoundException e) {
		        	System.out.println("Error archivo no encontrado");
		            e.printStackTrace();
		            return false;
		        } catch (IOException e) {
		        	System.out.println("Error de entrada/salida");
		            e.printStackTrace();
		            return false;
		        } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
			 
			 return true;
	}
	
	
}
