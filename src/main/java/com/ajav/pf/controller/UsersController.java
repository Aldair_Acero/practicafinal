package com.ajav.pf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ajav.pf.beans.Registro;
import com.ajav.pf.exceptions.CustomException;
import com.ajav.pf.interfaces.RegistroService;

@CrossOrigin
@RestController
public class UsersController {
	
	@Autowired
	private RegistroService registroService;
	
	@RequestMapping(value = "/api/v1/users/{IS}/{mes}", method = RequestMethod.GET)
	public List<Integer> getHorasIsMes(@PathVariable(value="IS") String IS, @PathVariable(value="mes") String mes) {
		List<Integer> list = registroService.findHorasByIs_empAndMes(IS, mes);
		
		if(list.isEmpty()) throw new CustomException(IS,mes);
		
		return list;
	}
	
	@RequestMapping(value = "/api/v1/users", method = RequestMethod.POST)
	public List<Registro> getRegistrosISEntreFechas(@RequestBody Registro registro) {
		System.out.println(" ---------" + registro.toString());
		return registroService.findAllRegistroByIs_empAndFechas(registro.getIs_emp(), registro.getFecha_entrada(), registro.getFecha_salida());
	}
	
}
