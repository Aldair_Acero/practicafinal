package com.ajav.pf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ajav.pf.beans.Registro;
import com.ajav.pf.interfaces.RegistroService;

@RestController
public class RegistroController {
	
	@Autowired
	private RegistroService registroService;
	
	 @RequestMapping(value = "/api/v1/getRegistros", method = RequestMethod.GET)
	public List <Registro> getRegistros() {
		return registroService.getRegistros();
	}
	
	 @RequestMapping(value = "/api/v1/addRegistro", method = RequestMethod.POST)
	public void addRegistro(@RequestBody Registro registro) {
		registroService.insertRegistro(registro);
	}
	 
}
