package com.ajav.pf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajav.pf.beans.Usuario;
import com.ajav.pf.interfaces.UserService;


@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public List <Usuario> test() {
		return userService.getUsers();
	}
	
	@PostMapping
	public void addUser(@RequestBody Usuario user) {
		userService.insertUser(user);
	}
	
}
