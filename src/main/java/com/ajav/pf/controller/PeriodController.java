package com.ajav.pf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ajav.pf.beans.Registro;
import com.ajav.pf.exceptions.CustomException;
import com.ajav.pf.interfaces.RegistroService;

@CrossOrigin
@RestController
public class PeriodController {
	@Autowired
	private RegistroService registroService;

	@RequestMapping(value = "/api/v1/period/{mes}", method = RequestMethod.GET)
	public List<Registro> getRegistrosPorMes(@PathVariable(value="mes") String mes) {
		List<Registro> list = registroService.findAllRegistroByMES(mes);
		
		if(list.isEmpty()) throw new CustomException(mes);
		
		return list;
	}
	
	@RequestMapping(value = "/api/v1/period", method = RequestMethod.POST)
	public List<Registro> getRegistrosPorFechas(@RequestBody Registro registro) {
		List<Registro> list = registroService.findAllRegistroByFechas(
				registro.getFecha_entrada(),
				registro.getFecha_salida()
				);
			
		if(list.isEmpty()) throw new CustomException(
				registro.getFecha_entrada(),
				registro.getFecha_salida(),
				1);
		
		return list;
	}
}
