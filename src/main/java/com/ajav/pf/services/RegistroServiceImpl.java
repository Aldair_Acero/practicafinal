package com.ajav.pf.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajav.pf.beans.Registro;
import com.ajav.pf.exceptions.CustomException;
import com.ajav.pf.interfaces.RegistroService;
import com.ajav.pf.repositories.RepoRegistros;

@Service
public class RegistroServiceImpl implements RegistroService{
	
	@Autowired
	private RepoRegistros repoRegistro;

	@Override
	public void insertRegistro(Registro registro){
		repoRegistro.save(registro);
	}

	@Override
	public List<Registro> getRegistros() {
		return repoRegistro.findAll();
	}
	
	@Override
	public List<Integer> findHorasByIs_empAndMes(String is_emp, String mes){
		
		return repoRegistro.findHorasByIs_empAndMes(is_emp, mes);
			
	}

	@Override
	public List<Registro> findAllRegistroByFechas(String fecha_inicial, String fecha_final) {
		// TODO Auto-generated method stub
		return repoRegistro.findAllRegistroByFechas(fecha_inicial, fecha_final);
	}

	@Override
	public List<Registro> findAllRegistroByMES(String mes) {
		// TODO Auto-generated method stub
		return repoRegistro.findAllRegistroByMES(mes);
	}

	@Override
	public List<Registro> findAllRegistroByIs_empAndFechas(String is_emp, String fecha_inicial, String fecha_final) {
		// TODO Auto-generated method stub
		return repoRegistro.findAllRegistroByIs_empAndFechas(is_emp, fecha_inicial, fecha_final);
	}
}
