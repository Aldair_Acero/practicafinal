package com.ajav.pf.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajav.pf.beans.Usuario;
import com.ajav.pf.interfaces.UserService;
import com.ajav.pf.repositories.RepoUsuarios;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private RepoUsuarios repoUsuarios;
	
	@Override
	public List<Usuario> getUsers(){
		return repoUsuarios.findAll();
	}
	
	@Override
	public void insertUser(Usuario user) {
		repoUsuarios.save(user);
		
	}

}
