//package com.ajav.pf;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import java.util.List;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import com.ajav.pf.beans.Registro;
//import com.ajav.pf.interfaces.RegistroService;
//import com.ajav.pf.repositories.RepoRegistros;
//
//
//@SpringBootTest
//class ProyectFinalApplicationTests {
//	
//	@Autowired
//	RegistroService registroService;
//	@Autowired
//	RepoRegistros repoRegistro;
//
//	@Test
//	void contextLoads() {
//	}
//	
////---------------------Pruebas unitarias-----------------------------------
//	
//	@Test
//	void findHorasByIs_empAndMesTest() {
//			String is_emp = "CXMG";
//			String mes = "01";
//			Integer valueExpected = 170;
//			List <Integer> horas = repoRegistro.findHorasByIs_empAndMes(is_emp, mes);
//			assertEquals(valueExpected,horas.get(0));
//		}
//	
//	@Test
//	void findAllRegistroByFechasTest() {
//		String fecha_inicial = "2019-01-19";
//		String fecha_final = "2019-02-19";
//		Registro registroExpected = new Registro(13,"CXMG","2019-01-21 08:09:12","2019-01-21 17:31:45",9);
//		List<Registro> lista = repoRegistro.findAllRegistroByFechas(fecha_inicial, fecha_final);
//		assertEquals(registroExpected.getId_registro(),lista.get(0).getId_registro());
//	}
//	
//	@Test
//	void baseDeDatosTest() {
//		int expected = 1932;
//		List<Registro> lista = repoRegistro.findAll();
//		assertFalse(lista.isEmpty());
//		assertEquals(expected, lista.size());
//	}
//	
//	
//		
//	
////---------------------Prueba de integración--------------------------------
//	
//	@Test
//	void findAllRegistroByIs_empAndFechasTest() {
//		String is_emp = "GAAN";
//		String fecha_inicial = "2019-01-19";
//		String fecha_final = "2019-02-19";
//		
//		List<Registro> registros = registroService.findAllRegistroByIs_empAndFechas(is_emp, fecha_inicial, fecha_final);
//		
//		Registro correctRegister = new Registro(137,"GAAN","2019-01-22 07:43:22", "2019-01-22 15:20:11", 7);
//		
//		assertEquals(correctRegister.getFecha_entrada(),registros.get(0).getFecha_entrada());
//	}
//	
//	
//
//}
